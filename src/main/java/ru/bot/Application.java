package ru.bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    static {
        var libs = System.getProperty("java.library.path");
        System.load(libs + "/libc++abi.so.1");
        System.load(libs + "/libc++.so.1");
        System.load(libs + "/libcrypto.so.1.0.0");
        System.load(libs + "/libssl.so.1.0.0");
        System.load(libs + "/libtdjni.so");
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
