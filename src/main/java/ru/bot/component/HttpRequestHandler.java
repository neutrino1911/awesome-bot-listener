package ru.bot.component;

import com.sun.net.httpserver.HttpExchange;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.bot.component.event.publisher.SystemEventPublisher;

import java.io.IOException;

@Slf4j
@Component
@RequiredArgsConstructor
public class HttpRequestHandler {

    private final SystemEventPublisher systemEventPublisher;

    public void handle(HttpExchange exchange) throws IOException {
        var path = exchange.getRequestURI().getPath();
        log.debug("handle path: {}", path);
        exchange.sendResponseHeaders(200, 0);
        exchange.close();
        var split = path.split("/");
        if (split.length < 3) {
            return;
        }
        switch (split[1]) {
            case "sendCode":
                systemEventPublisher.publishEvent("authCode " + split[2]);
                break;
            case "sendPassword":
                systemEventPublisher.publishEvent("password " + split[2]);
                break;
        }
    }

}
