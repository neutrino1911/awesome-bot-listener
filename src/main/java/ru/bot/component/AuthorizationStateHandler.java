package ru.bot.component;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.drinkless.tdlib.Client;
import org.drinkless.tdlib.TdApi;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bot.component.event.SystemEvent;
import ru.bot.component.event.publisher.ClientSendPublisher;
import ru.bot.component.event.publisher.SystemEventPublisher;
import ru.bot.config.TelegramApiProperties;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthorizationStateHandler implements Client.ResultHandler {

    private final ClientSendPublisher clientSendPublisher;

    private final SystemEventPublisher systemEventPublisher;

    private final TelegramApiProperties props;

    private TdApi.AuthorizationState authorizationState;

    public void onUpdate(TdApi.AuthorizationState authorizationState) {
        if (authorizationState != null) {
            this.authorizationState = authorizationState;
        }
        log.debug("authorization state: {}", this.authorizationState.getClass().getSimpleName());
        switch (this.authorizationState.getConstructor()) {
            case TdApi.AuthorizationStateWaitTdlibParameters.CONSTRUCTOR:
                TdApi.TdlibParameters parameters = new TdApi.TdlibParameters();
                parameters.databaseDirectory = format("%s/%s/tdlib", props.getPath(), props.getPhone());
                parameters.useChatInfoDatabase = true;
                parameters.useMessageDatabase = true;
                parameters.apiId = props.getAppId();
                parameters.apiHash = props.getAppHash();
                parameters.systemLanguageCode = "en";
                parameters.deviceModel = "Desktop";
                parameters.systemVersion = "Unknown";
                parameters.applicationVersion = "1.0";
                parameters.enableStorageOptimizer = true;

                clientSendPublisher.publishEvent(new TdApi.SetTdlibParameters(parameters), this);
                break;
            case TdApi.AuthorizationStateWaitEncryptionKey.CONSTRUCTOR:
                clientSendPublisher.publishEvent(new TdApi.CheckDatabaseEncryptionKey(), this);
                break;
            case TdApi.AuthorizationStateWaitPhoneNumber.CONSTRUCTOR:
                clientSendPublisher.publishEvent(
                        new TdApi.SetAuthenticationPhoneNumber(props.getPhone(), false, false),
                        this
                );
                break;
            case TdApi.AuthorizationStateWaitCode.CONSTRUCTOR: {
                systemEventPublisher.publishEvent("waitAuthCode");
                break;
            }
            case TdApi.AuthorizationStateWaitPassword.CONSTRUCTOR: {
                systemEventPublisher.publishEvent("waitPassword");
                break;
            }
            case TdApi.AuthorizationStateReady.CONSTRUCTOR:
                systemEventPublisher.publishEvent("authorized");
                log.debug("Logging in");
                break;
            case TdApi.AuthorizationStateLoggingOut.CONSTRUCTOR:
                systemEventPublisher.publishEvent("unauthorized");
                log.debug("Logging out");
                break;
            case TdApi.AuthorizationStateClosing.CONSTRUCTOR:
                systemEventPublisher.publishEvent("unauthorized");
                log.debug("Closing");
                break;
            case TdApi.AuthorizationStateClosed.CONSTRUCTOR:
                log.debug("Closed");
                break;
            default:
                log.error("Unsupported authorization state: {}", this.authorizationState);
        }
    }

    @Override
    @SneakyThrows(InterruptedException.class)
    public void onResult(TdApi.Object object) {
        switch (object.getConstructor()) {
            case TdApi.Error.CONSTRUCTOR:
                var error = (TdApi.Error) object;
                log.error("Receive an error: {}:{}", error.code, error.message);
                if (error.code == 429) {
                    Thread.sleep(Long.parseLong(error.message.substring(31)) * 1000 + 1000);
                }
                onUpdate(null);
                break;
            case TdApi.Ok.CONSTRUCTOR:
                // result is already received through UpdateAuthorizationState, nothing to do
                break;
            default:
                log.error("Receive wrong response from TDLib: {}", object);
        }
    }

    @EventListener(classes = SystemEvent.class, condition = "event.what matches '^authCode \\d{5}$'")
    public void handleAuthCode(SystemEvent event) {
        log.debug("handleAuthCode: {}", event.getWhat());
        var code = event.getWhat().substring(9);
        clientSendPublisher.publishEvent(new TdApi.CheckAuthenticationCode(code, "", ""), this);
    }

    @EventListener(classes = SystemEvent.class, condition = "event.what matches '^password .+'")
    public void handlePassword(SystemEvent event) {
        log.debug("handlePassword: {}", event.getWhat());
        var password = event.getWhat().substring(9);
        clientSendPublisher.publishEvent(new TdApi.CheckAuthenticationPassword(password), this);
    }

}
