package ru.bot.component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.drinkless.tdlib.Client;
import org.drinkless.tdlib.TdApi;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.bot.component.event.NewMessageEvent;
import ru.bot.component.event.SystemEvent;

@Slf4j
@Component
@RequiredArgsConstructor
public class UpdatesHandler implements Client.ResultHandler {

    private final AuthorizationStateHandler authorizationStateHandler;

    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void onResult(TdApi.Object object) {
        switch (object.getConstructor()) {
            case TdApi.Error.CONSTRUCTOR: {
                var error = (TdApi.Error) object;
                log.error("{}:{}", error.code, error.message.replace('\n', ' '));
                break;
            }
            case TdApi.UpdateNewMessage.CONSTRUCTOR: {
                var newMessage = (TdApi.UpdateNewMessage) object;
                applicationEventPublisher.publishEvent(new NewMessageEvent(this, newMessage.message));
                break;
            }
            case TdApi.UpdateAuthorizationState.CONSTRUCTOR: {
                var updateAuthorizationState = (TdApi.UpdateAuthorizationState) object;
                var authorizationState = updateAuthorizationState.authorizationState;
                publishSystemEvent(object, authorizationState);
                authorizationStateHandler.onUpdate(authorizationState);
                break;
            }
            case TdApi.UpdateConnectionState.CONSTRUCTOR: {
                var updateConnectionState = (TdApi.UpdateConnectionState) object;
                var state = updateConnectionState.state;
                publishSystemEvent(object, state);
                break;
            }
        }
    }

    private void publishSystemEvent(TdApi.Object parent, TdApi.Object child) {
        var event = parent.getClass().getSimpleName() + '.' + child.getClass().getSimpleName();
        log.debug("publishSystemEvent: {}", event);
        applicationEventPublisher.publishEvent(new SystemEvent(this, event));
    }

}
