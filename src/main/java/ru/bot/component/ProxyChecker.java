package ru.bot.component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.drinkless.tdlib.TdApi;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bot.component.event.SystemEvent;
import ru.bot.component.event.publisher.ClientSendPublisher;
import ru.bot.config.TelegramApiProperties;

import java.util.Arrays;

@Slf4j
@Component
@RequiredArgsConstructor
public class ProxyChecker {

    private final TelegramApiProperties props;

    private final ClientSendPublisher clientSendPublisher;

    @EventListener(classes = SystemEvent.class, condition = "event.what == 'UpdateConnectionState.ConnectionStateConnecting'")
    public void run(SystemEvent event) {
        log.debug("run");
        clientSendPublisher.publishEvent(new TdApi.GetProxies(), obj -> {
            log.debug("run obj: {}", String.valueOf(obj).replace('\n', ' '));
            var proxies = (TdApi.Proxies) obj;
            log.debug("run proxies: {}", Arrays.toString(proxies.proxies).replace('\n', ' '));
            for (var proxy : proxies.proxies) {
                if (proxy.server.equals(props.getProxy().getServer())
                        && proxy.port == props.getProxy().getPort()
                        && proxy.type.getConstructor() == TdApi.ProxyTypeMtproto.CONSTRUCTOR
                        && ((TdApi.ProxyTypeMtproto) proxy.type).secret.equals(props.getProxy().getSecret())) {
                    if (proxy.isEnabled) {
                        return;
                    } else {
                        clientSendPublisher.publishEvent(new TdApi.EnableProxy(proxy.id), null);
                        return;
                    }
                }
            }

            var addProxy = new TdApi.AddProxy();
            addProxy.server = props.getProxy().getServer();
            addProxy.port = props.getProxy().getPort();
            addProxy.type = new TdApi.ProxyTypeMtproto(props.getProxy().getSecret());
            addProxy.enable = true;
            clientSendPublisher.publishEvent(addProxy, null);
        });
    }

}
