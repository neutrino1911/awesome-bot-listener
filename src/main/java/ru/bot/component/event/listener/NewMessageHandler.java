package ru.bot.component.event.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.drinkless.tdlib.TdApi;
import org.springframework.context.event.EventListener;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import ru.bot.component.event.NewMessageEvent;
import ru.bot.config.BotProperties;
import ru.bot.data.Message;

@Slf4j
@Component
@RequiredArgsConstructor
public class NewMessageHandler {

    private final BotProperties props;

    private final ObjectMapper objectMapper;

    private final StringRedisTemplate stringRedisTemplate;

    @EventListener(NewMessageEvent.class)
    public void onApplicationEvent(NewMessageEvent event) {
        var message = event.getMessage();
        if (!(message.content instanceof TdApi.MessageText)) {
            return;
        }

        var content = (TdApi.MessageText) message.content;

        if (message.chatId == props.getChatId()) {
            handleMessage(message.id, message.chatId, message.date, content.text.text);
        } else if (message.forwardInfo != null
                && message.forwardInfo.origin instanceof TdApi.MessageForwardOriginChannel) {
            var origin = (TdApi.MessageForwardOriginChannel) message.forwardInfo.origin;
            if (origin.chatId == props.getChatId()) {
                handleMessage(origin.messageId, message.chatId, message.forwardInfo.date, content.text.text);
            }
        }
    }

    @SneakyThrows
    private void handleMessage(long id, long chatId, int date, String text) {
        var msg = new Message(id, chatId, date, text);
        log.debug("onApplicationEvent msg: {}", msg.toString().replace('\n', ' '));
        stringRedisTemplate.convertAndSend("chat_msg", objectMapper.writeValueAsString(msg));
    }

}
