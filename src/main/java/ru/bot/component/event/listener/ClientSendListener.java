package ru.bot.component.event.listener;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.drinkless.tdlib.Client;
import org.drinkless.tdlib.TdApi;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.bot.component.UpdatesHandler;
import ru.bot.component.event.ClientSendEvent;
import ru.bot.config.TelegramApiProperties;

import java.io.IOError;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
public class ClientSendListener implements ApplicationRunner {

    private final TelegramApiProperties props;

    private final UpdatesHandler updatesHandler;

    private Client client;

    @EventListener(ClientSendEvent.class)
    public void onApplicationEvent(ClientSendEvent event) {
        if (client != null) {
            client.send(event.getQuery(), event.getResultHandler());
        } else {
            log.error("client not initialized: {}", event.getQuery());
        }
    }

    @Override
    @SneakyThrows(IOException.class)
    public void run(ApplicationArguments args) {
        Client.execute(new TdApi.SetLogVerbosityLevel(1));
        var dir = format("%s/%s/", props.getPath(), props.getPhone());
        if (!Files.exists(Path.of(dir))) {
            Files.createDirectory(Path.of(dir));
        }
        var file = format("%stdlib.log", dir);
        var res = Client.execute(new TdApi.SetLogStream(new TdApi.LogStreamFile(file, 1 << 27)));
        if (res instanceof TdApi.Error) {
            throw new IOError(new IOException(format("Write access to %s is required", file)));
        }
        this.client = Client.create(updatesHandler, this::errorHandler, this::errorHandler);
    }

    private void errorHandler(Throwable t) {
        log.error(t.getMessage(), t);
    }

}
