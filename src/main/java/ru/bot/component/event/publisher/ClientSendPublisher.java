package ru.bot.component.event.publisher;

import lombok.RequiredArgsConstructor;
import org.drinkless.tdlib.Client;
import org.drinkless.tdlib.TdApi;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.bot.component.event.ClientSendEvent;

@Component
@RequiredArgsConstructor
public class ClientSendPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishEvent(TdApi.Function query, Client.ResultHandler resultHandler) {
        applicationEventPublisher.publishEvent(new ClientSendEvent(this, query, resultHandler));
    }

}
