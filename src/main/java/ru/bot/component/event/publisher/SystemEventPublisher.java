package ru.bot.component.event.publisher;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.bot.component.event.SystemEvent;

@Component
@RequiredArgsConstructor
public class SystemEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishEvent(String what) {
        applicationEventPublisher.publishEvent(new SystemEvent(this, what));
    }

}
