package ru.bot.component.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class SystemEvent extends ApplicationEvent {

    private String what;

    public SystemEvent(Object source, String what) {
        super(source);
        this.what = what;
    }

}
