package ru.bot.component.event;

import lombok.Getter;
import org.drinkless.tdlib.TdApi;
import org.springframework.context.ApplicationEvent;

@Getter
public class NewMessageEvent extends ApplicationEvent {

    private TdApi.Message message;

    public NewMessageEvent(Object source, TdApi.Message message) {
        super(source);
        this.message = message;
    }

}
