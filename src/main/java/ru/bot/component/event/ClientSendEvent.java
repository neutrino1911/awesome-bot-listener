package ru.bot.component.event;

import lombok.Getter;
import org.drinkless.tdlib.Client;
import org.drinkless.tdlib.TdApi;
import org.springframework.context.ApplicationEvent;

@Getter
public class ClientSendEvent extends ApplicationEvent {

    private TdApi.Function query;

    private Client.ResultHandler resultHandler;

    public ClientSendEvent(Object source, TdApi.Function query, Client.ResultHandler resultHandler) {
        super(source);
        this.query = query;
        this.resultHandler = resultHandler;
    }

}
