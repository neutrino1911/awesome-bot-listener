package ru.bot.config;

import com.sun.net.httpserver.HttpServer;
import lombok.SneakyThrows;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.bot.component.HttpRequestHandler;

import java.net.InetSocketAddress;

@Configuration
@EnableConfigurationProperties({TelegramApiProperties.class, BotProperties.class})
public class AppConfig {

    @Bean
    @SneakyThrows
    public HttpServer httpServer(TelegramApiProperties props, HttpRequestHandler httpRequestHandler) {
        var server = HttpServer.create(new InetSocketAddress(props.getPort()), 0);
        var context = server.createContext("/");
        context.setHandler(httpRequestHandler::handle);
        server.start();
        return server;
    }

}
