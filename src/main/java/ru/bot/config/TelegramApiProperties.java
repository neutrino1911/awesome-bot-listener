package ru.bot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Validated
@ConfigurationProperties("tg-api")
public class TelegramApiProperties {

    @NotNull
    private Integer appId;

    @NotEmpty
    private String appHash;

    @NotEmpty
    private String phone;

    @NotEmpty
    private String path;

    private int port = 8080;

    private Proxy proxy = new Proxy();

    @Data
    @Validated
    public static class Proxy {

        private String server;

        private int port;

        private String secret;

    }

}
