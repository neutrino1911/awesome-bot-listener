FROM openjdk:11

ADD build/libs/twinsingle-bot-listener-1.0.jar /app.jar

ADD lib/* /usr/lib/

RUN mkdir /data

EXPOSE 8080

CMD ["java", "-jar", "/app.jar"]
